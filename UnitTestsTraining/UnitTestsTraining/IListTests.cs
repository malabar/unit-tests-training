using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace UnitTestsTraining
{
    public class IListTests
    {
        [Fact]
        public void ShouldFindIndexOfElementInNotEmptyList()
        {
            var list = new List<int> {1, 2, 3};
            var elementToFind = 2;
            var expected = 1;

            //act
            var actual = list.IndexOf(elementToFind);

            //assert
            actual.Should().Be(expected);
        }

        [Fact]
        public void ShouldNotFindIndexOfElementInNoTEmptyList()
        {
            var list = new List<int> { 1, 2, 3 };
            var elementToFind = 4;
            var expected = -1;

            //act
            var actual = list.IndexOf(elementToFind);

            //assert
            actual.Should().Be(expected);
        }

        [Fact]
        public void ShouldNotFindIndexOfElementInEmptyList()
        {
            var emptyList = new List<int>();
            var elementToFind = 1;
            var expected = -1;

            //act
            var actual = emptyList.IndexOf(elementToFind);

            //assert
            actual.Should().Be(expected);
        }

        public static IEnumerable<object[]> InsertElementIntoListData => new List<object[]>
        {
            new object[] {new List<int> {1, 2, 3}, 0, 4, new List<int> {4, 1, 2, 3}},
            new object[] {new List<int> {1, 2, 3}, 1, 4, new List<int> {1, 4, 2, 3}},
            new object[] {new List<int> {1, 2, 3}, 3, 4, new List<int> {1, 2, 3, 4}}
        };

        [Theory]
        [MemberData(nameof(InsertElementIntoListData))]
        public void ShouldInsertElementIntoList(List<int> sut, int insertIndex, int element, List<int> expected)
        {
            //act
            sut.Insert(insertIndex, element);

            //assert
            sut.Should().BeEquivalentTo(expected);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(4)]
        public void ShouldNotInsertElementOutOfRangeIntoList(int insertIndex)
        {
            var sut = new List<int> {1, 2, 3};
            var expected = new List<int> {1, 2, 3};
            var element = 4;

            //act
            Action insert = () => sut.Insert(insertIndex, element);

            //assert
            insert.Should().ThrowExactly<ArgumentOutOfRangeException>();
            sut.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ShouldNotInsertElementIntoReadonlyList()
        {
            var sut = (IList<int>)new List<int> {1, 2, 3}.AsReadOnly();
            var expected = new List<int> { 1, 2, 3 };
            var element = 4;
            var index = 1;

            //act
            Action insert = () => sut.Insert(index, element);

            //assert
            sut.IsReadOnly.Should().BeTrue();
            insert.Should().ThrowExactly<NotSupportedException>();
            sut.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ShouldNotRemoveElementFromReadonlyList()
        {
            var sut = (IList<int>)new List<int> { 1, 2, 3 }.AsReadOnly();
            var expected = new List<int> {1, 2, 3};
            var index = 1;

            //act
            Action insert = () => sut.Remove(index);

            //assert
            sut.IsReadOnly.Should().BeTrue();
            insert.Should().ThrowExactly<NotSupportedException>();
            sut.Should().BeEquivalentTo(expected);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(3)]
        [InlineData(4)]
        public void ShouldNotRemoveElementOutOfRangeFromList(int index)
        {
            var actual = new List<int> {1, 2, 3};
            var expected = new List<int> {1, 2, 3};

            index.Should().NotBeInRange(0, actual.Count - 1); // validates input data

            //act
            Action remove = () => actual.RemoveAt(index);

            //assert
            remove.Should().ThrowExactly<ArgumentOutOfRangeException>();
            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<object[]> RemoveElementsFromListData => new List<object[]>
        {
            new object[] {new List<int> {1, 2, 3}, 0, new List<int> {2, 3}},
            new object[] {new List<int> {1, 2, 3}, 1, new List<int> {1, 3}},
            new object[] {new List<int> {1, 2, 3}, 2, new List<int> {1, 2}}
        };

        [Theory]
        [MemberData(nameof(RemoveElementsFromListData))]
        public void ShouldRemoveElementsFromList(List<int> actual, int index, List<int> expected)
        {
            var initialCount = actual.Count;

            //act
            actual.RemoveAt(index);

            //assert
            index.Should().BeInRange(0, initialCount - 1);
            actual.Should().BeEquivalentTo(expected);
        }
    }
}
